from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import classification_report as clsr
from sklearn.linear_model import LogisticRegression
from sklearn import datasets
from sklearn.metrics import roc_auc_score
from sklearn import svm
import numpy as np

def extractLinearSVC(Document,Label):
    print("creating model...")
    #Separation into training documents and test documents
    document_train, document_test, labels_train, labels_test = tts(Document, Label, test_size=0.2)
    #vectorisation without preprocessing (already done)
    vect = TfidfVectorizer(min_df=4, ngram_range = (1,4), preprocessor=None, lowercase=False).fit(document_train)
    #Print of the words
    print("Number of feature words :")
    print(len(vect.get_feature_names()))
    print()
    #document in vector form |(phrase,word) -positive and negative coefficients-|
    document_train_vectorized = vect.transform(document_train)
    #model creation
    model = svm.LinearSVC()
    model.fit(document_train_vectorized, labels_train)
    # Test on the tts
    predictions = model.predict(vect.transform(document_test))
    # print results
    print(clsr(labels_test, predictions))

    feature_names = np.array(vect.get_feature_names())
    sorted_coef_index = model.coef_[0].argsort()
    print('Smallest Coefs: \n{}\n'.format(feature_names[sorted_coef_index[:20]]))
    print('Largest Coefs: \n{}\n'.format(feature_names[sorted_coef_index[:-21:-1]]))


def extractLinearSVCrendu(Document, Documenttotest, Label):
    print("creating model...")
    vect = TfidfVectorizer(min_df=3, ngram_range = (1,4), preprocessor=None, lowercase=False).fit(Document)
    #Print of the words
    print("Number of feature words :")
    print(len(vect.get_feature_names()))
    print()
    document_vectorized = vect.transform(Document)
    #model creation
    model = svm.LinearSVC()
    model.fit(document_vectorized, Label)
    # Test on the tts
    predictions = model.predict(vect.transform(Documenttotest))
    filename = "predictions.csv"
    thefile = open(filename, 'w')
    for item in predictions:
        thefile.write("%s\n" % item)

    feature_names = np.array(vect.get_feature_names())
    sorted_coef_index = model.coef_[0].argsort()
    print('Smallest Coefs: \n{}\n'.format(feature_names[sorted_coef_index[:20]]))
    print('Largest Coefs: \n{}\n'.format(feature_names[sorted_coef_index[:-21:-1]]))



def encodeLabels(Label):
    labels = LabelEncoder()
    return labels.fit_transform(Label)
from extractSVCLinear import extractSVCLinear
from preprocessing import NLTKPreprocessor
from functions import preprocessAndOutText, preprocessText, getPolarities, loadPreprocessedText, createpreprocessAndOutText
from extraction import extract, extractToDocument
from extractionNaiveBayes import extractNaiveBayes
from LinearSVC import extractLinearSVC
from compareModel import extractToDocumentAll
import glob

def main():
    polarities = getPolarities("labels.csv")
    # pour thibault
    #names = glob.glob("datasetthibault/*.txt")
    # pour ismael
    names = glob.glob("datasetismael/*.txt")
    # pour faniry
    #names = glob.glob("datasetfaniry/*.txt")
    # pour abdou
    # names = glob.glob("datasetabdou/*.txt")
    # pour raoul
    #names = glob.glob("step1/*.txt")
    # print(names)
    # polarities = getPolarities("labels.csv")
    # pour thibault
    #thefile = open('datasetthibault/resultSVCLinear.csv', 'w')
    # pour ismael
    thefile = open('datasetismael/resultSVCLinear.csv', 'w')
    # pour faniry
    #thefile = open('datasetfaniry/resultSVCLinear2.csv', 'w')
    # pour abdou
    # thefile = open('datasetabdou/resultSVCLinear.csv', 'w')
    # pour raoul
    #thefile = open('step1/resultSVCLinear.csv', 'w')
    x = 1
    print("starting...")
    for name in names:
        text = loadPreprocessedText(name)
        resultset = extractSVCLinear(text,polarities)
        thefile.write("%s%s\n" % (name,resultset))
        print(name)
        print(x)
        print("/26 percent finished")
        x = x+1
    print("finished")

if __name__ == "__main__":
    main()
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import classification_report as clsr
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from sklearn import svm
from sklearn.naive_bayes import BernoulliNB
import numpy as np

def extractToDocumentAll(Document,Label):
    results = ""
    for df in range(0, 7):
        for ngr in range(1, 5):
            name = ",%s,%s" % (df,ngr)
            results = results + name
            print(name)
            for times in range(0, 1):
                if(ngr == 1):
                    Labels = encodeLabels(Label)
                    document_train, document_test, labels_train, labels_test = tts(Document, Labels, test_size=0.2)
                    vect = TfidfVectorizer(min_df=df, preprocessor=None, lowercase=False).fit(document_train)
                    document_train_vectorized = vect.transform(document_train)
                    model = LogisticRegression()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
                    model = BernoulliNB()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
                    model = svm.LinearSVC()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
                if(ngr!=1):
                    Labels = encodeLabels(Label)
                    document_train, document_test, labels_train, labels_test = tts(Document, Labels, test_size=0.2)
                    vect = TfidfVectorizer(min_df=df, ngram_range=(1, ngr), preprocessor=None, lowercase=False).fit(
                        document_train)
                    document_train_vectorized = vect.transform(document_train)
                    model = LogisticRegression()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
                    model = BernoulliNB()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
                    model = svm.LinearSVC()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
    return results




def encodeLabels(Label):
    labels = LabelEncoder()
    return labels.fit_transform(Label)
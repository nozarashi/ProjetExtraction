import string
#imports stopwords
from nltk.corpus import stopwords as sw
#imports word net tags
from nltk.corpus import wordnet as wn
#Break the document into sentences
from nltk import wordpunct_tokenize
#gets singular
from nltk import WordNetLemmatizer
#Break the sentence into part of speech
from nltk import sent_tokenize
#tags for each word, diffrent tags can be found here  -> https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
from nltk import pos_tag

from sklearn.base import BaseEstimator, TransformerMixin


class NLTKPreprocessor(BaseEstimator, TransformerMixin):

    #initilizer
    def __init__(self, stopwords=None, punct=None, counter=1,
                 lower=False, strip=True, lemmatiz=False, names=False, numbers=True, adv=False, conjonction=True):
        self.lower = lower
        self.strip = strip
        self.stopwords = stopwords or set(sw.words('english'))
        self.punct = punct or set(string.punctuation)
        self.lemmatizer = WordNetLemmatizer()
        self.counter = counter
        self.lemmatiz = lemmatiz
        self.names = names
        self.numbers = numbers
        self.adv = adv
        self.conjonction = conjonction

    #turn back into one document
    #returns one document
    def inverse_transform(self, X):
        return [" ".join(doc) for doc in X]

    # seperate each document
    # returns list of list of words that have been tokenized
    def transform(self, X):
            print("Total number of texts ", len(X))
            return [
                list(self.tokenize(doc)) for doc in X
            ]


    # tokenize + lemmitize + delete names words
    # returns words of the text
    def tokenize(self, document):
        # print text number
        if (self.counter%1000==0):
            print("text number ", self.counter)
        self.counter = self.counter + 1

        # Break the document into sentences
        for sent in sent_tokenize(document):

            # Break the sentence into part of speech tagged tokens
            for token, tag in pos_tag(wordpunct_tokenize(sent)):

                # Apply preprocessing to the tokens
                token = token.lower() if self.lower else token
                token = token.strip() if self.strip else token
                token = token.strip('_') if self.strip else token
                token = token.strip('*') if self.strip else token

                # If word is a Proper Noun plural or Proper noun singular, ignore token and continue
                if (self.names):
                    if (tag == "NNP" or tag == "NNPS"):
                        continue

                # If word is a Adverb, comparative
                if (self.adv):
                    if (tag == "RBR"):
                        continue

                # If word is a number
                if (self.numbers):
                    if (tag == "CD"):
                        continue

                # If word is a number
                if (self.numbers):
                    if (tag == "CC"):
                        continue

                # If stopword, ignore token and continue
                if token in self.stopwords:
                    continue

                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                    continue

                # Lemmatize the token and yield
                token = self.lemmatize(token, tag) if self.lemmatiz else token
                yield token



    #transforms Noun, Verb, Adverb, Adjective into Noun
    def lemmatize(self, token, tag):
        tag = {
            'N': wn.NOUN,
            'V': wn.VERB,
            'R': wn.ADV,
            'J': wn.ADJ
        }.get(tag[0], wn.NOUN)

        return self.lemmatizer.lemmatize(token, tag)

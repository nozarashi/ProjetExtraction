from extractSVCLinear import extractSVCLinear
from preprocessing import NLTKPreprocessor
from functions import preprocessAndOutText, preprocessText, getPolarities, loadPreprocessedText, createpreprocessAndOutText
from extraction import extract, extractToDocument
from extractionNaiveBayes import extractNaiveBayes
from LinearSVC import extractLinearSVC, extractLinearSVCrendu
from compareModel import extractToDocumentAll
import glob


def main():
    polarities = getPolarities("labels.csv")
    text = loadPreprocessedText("fulldataset/outputlowerFalsestripTruelemmatizFalsenamesFalsenumbersTrueadverbsFalseconjoctionTrue.txt")
    texttotest = preprocessText("test_data.csv")
    extractLinearSVCrendu(text, texttotest, polarities)

if __name__ == "__main__":
    main()